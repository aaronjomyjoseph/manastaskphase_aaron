
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
data = pd.read_csv (r'C:\Users\Maximus\Desktop\car_price.csv')
data.dropna(inplace=True)
data.drop(['torque'], axis=1, inplace=True)
def removeunits(x):
    final = ''
    for i in x:
            if i.isdigit() or i=='.':
                final += str(i)
    return float(final)
names={"Ambassador": 1, "Ashok": 2, "Audi": 3, "BMW": 4, "Chevrolet": 5, "Daewoo": 6, "Datsun": 7, "Fiat": 8, "Force": 9, "Ford": 10, "Honda": 11, "Hyundai": 12, "Isuzu": 13, "Jaguar": 14, "Jeep": 15, "Kia": 16, "Land": 17, "Lexus": 18, "Mahindra": 19,"Maruti": 20,"Mercedes-Benz": 21,"MG": 22,"Mitsubishi": 23,"Nissan": 24,"Opel": 25,"Peugeot": 26,"Renault": 27,"Skoda": 28,"Tata": 29,"Toyota": 30,"Volkswagen": 31,"Volvo": 32}
def nameclassify(x):
    x=x.split(' ', 1)[0]
    return float(names[x])
def ownerclassify(x):
    x=x.split(' ', 1)[0]
    if x=="First":
        x=1
    elif x=="Second":
        x=2
    elif x=="Third":
        x=3
    elif x=="Fourth":
        x=4
    elif x=="Test":
        x=5
    return float(x)
def sellerclassify(x):
    x=x.split(' ', 1)[0]
    if x=="Individual":
        x=1
    elif x=="Dealer":
        x=2
    elif x=="Trustmark":
        x=3
    return float(x)
def fuelclassify(x):
    x=x.split(' ', 1)[0]
    if x=="Petrol":
        x=1
    elif x=="Diesel":
        x=2
    elif x=="LPG":
        x=3
    elif x=="CNG":
        x=4
    return float(x)
def transmissionclassify(x):
    x=x.split(' ', 1)[0]
    if x=="Manual":
        x=1
    elif x=="Automatic":
        x=2
    return float(x)
data['mileage']=data['mileage'].apply(removeunits).astype("float")
data['engine']=data['engine'].apply(removeunits).astype("float")
data['max_power']=data['max_power'].apply(removeunits).astype("float")
data['year']=data['year'].apply(float).astype("float")
data['selling_price']=data['selling_price'].apply(float).astype("float")
data['km_driven']=data['km_driven'].apply(float).astype("float")
data['name']=data['name'].apply(nameclassify)
data['owner']=data['owner'].apply(ownerclassify)
data['seller_type']=data['seller_type'].apply(sellerclassify)
data['fuel']=data['fuel'].apply(fuelclassify)
data['transmission']=data['transmission'].apply(transmissionclassify)
m=data['name'].count()
l=[1.0]*m
data['new']=l
X=data.to_numpy()
y=X[:,2]
m=len(y)
X=np.delete(X, 2, 1)
k=np.shape(X)[1]
thet=np.array([[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1],[0.1]])
Xsc=X.copy()
minx=np.min(X[:,1:])
maxx=np.max(X[:,1:])
Xsc[:,1:]=(X[:,1:]-minx)/(maxx-minx)
ysc=y.copy()
maxy=np.max(y)
miny=np.min(y)
ysc=(y-miny)/(maxy-miny)
ysc=ysc.reshape(-1,1)
def grad(error):
    pd=(-Xsc.T).dot(error)
    return pd
def cost(error):
    J=np.array([])
    J = (1/(2*m))*np.sum(error**2,axis=0)[0]
    return J
J_history = []
epoch_history=[]
def regression(theta0,learning_rate=0.000001,epochs=100):
    print(f'epoch \t Cost(J) \t')
    for epoch in range(epochs):
        epoch_history.append(epoch)
        ypr=Xsc.dot(theta0)
        print(ypr)
        error=ysc-ypr
        dJ=grad(error)
        J=cost(error)
        theta0=theta0-learning_rate*dJ
        J_history.append(J)
        if epoch%10==0:
            print(f'{epoch:5d}\t{J_history[-1]:7.4f}\t')
    return theta0
newtheta=regression(thet)
ypr=Xsc.dot(newtheta)
ypr=ypr*(maxy-miny)+miny
print("original")
print(y)
print("predicted")
print(ypr)
plt.plot(epoch_history,J_history)
plt.show()
