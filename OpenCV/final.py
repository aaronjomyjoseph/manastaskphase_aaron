import cv2
import numpy as np
import matplotlib.pyplot as plt

def canny(img, low_threshold,high_threshold):
    cannyimg = cv2.Canny(img, low_threshold, high_threshold)
    
    return cannyimg

def gaussian_blur(img, k):
    blurimg = cv2.GaussianBlur(img, (k,k), 0)
    return blurimg

def grayscale(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray

def adjust_gamma(image, gamma):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)

def to_hls(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2HLS)

def isolate_color_mask(img, low_thresh, high_thresh):
    assert(low_thresh.all() >=0  and low_thresh.all() <=255)
    assert(high_thresh.all() >=0 and high_thresh.all() <=255)
    return cv2.inRange(img, low_thresh, high_thresh)

def region_of_interest(img):
    height=img.shape[0]
    width=img.shape[1]
    height -= 50
    width -= 50
    Polygons = np.array([
        [(width, height), (50,height), (int((3/8)*width), int((3/4)*height)),(int((5/8)*width), int((3/4)*height))]
           
    ])

    mask = np.zeros_like(img)
    cv2.fillConvexPoly(mask, Polygons, 255)
    maskimg = cv2.bitwise_and(img, mask)
    return maskimg

def binary_threshold(img):
    white_mask = isolate_color_mask(to_hls(img), np.array([0, 200, 0], dtype=np.uint8), np.array([200, 255, 255], dtype=np.uint8))
    yellow_mask = isolate_color_mask(to_hls(img), np.array([20, 120, 80], dtype=np.uint8), np.array([45, 200, 255], dtype=np.uint8))
    mask = cv2.bitwise_or(white_mask, yellow_mask)
    return mask


def get_cords(img, line_slope_int):
    slope, intercept = line_slope_int
    y1 = img.shape[0]
    y2 = int(y1 * (4/5)) # we only want the line to go 3.6 fifths of the way up
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    print(img.shape)
    height=img.shape[0]
    width=img.shape[1]
    if x1 > width or x1 < 0 or x2 > width or x2 < 0 or y1 > height or y1 < 0 or y2 > height or y2 < 0:
        return np.array([0,0,0,0])
    return np.array([x1,y1,x2,y2])

def average_slope_intercept(img, lines):
    left_fit = []
    right_fit = []
    if lines is None:
        return (np.array([0,0,0,0]), np.array([0,0,0,0]))
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        slope, intercept = np.polyfit((x1,x2), (y1,y2), 1)
        print(slope, intercept)
        #left lines negative left else right
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))

    if left_fit:
        left_fit_avg = np.average(left_fit, axis=0)
        left_line = get_cords(img, left_fit_avg)
    else:
        left_line = np.array([0,0,0,0])
    if right_fit:
        right_fit_avg = np.average(right_fit, axis=0)
        right_line = get_cords(img, right_fit_avg)
    else:
        right_line = np.array([0,0,0,0])

    return np.array([left_line, right_line])


def display_lines(img, lines):
    line_img = np.zeros_like(img)
    if lines is not None:
        for x1,y1,x2,y2 in lines:
            cv2.line(line_img, (x1,y1), (x2,y2), (0,255,0), 30)
    return line_img

cap = cv2.VideoCapture(r'C:\Users\Maximus\manastaskphase_aaron\data\challenge_video.mp4')
while(cap.isOpened()):
    _, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    gray_img = grayscale(frame)
    darkened= adjust_gamma(gray_img, 0.5)
    mask=binary_threshold(frame)
    maskedimg= cv2.bitwise_and(darkened, darkened, mask=mask)
    
    gaussian=gaussian_blur(maskedimg, k=7) 

    cannyimg=canny(gaussian, low_threshold=50, high_threshold=150)
    cropped_image=region_of_interest(cannyimg)
    lines = cv2.HoughLinesP(cropped_image, 2, np.pi / 180, 100,
                            np.array([]), minLineLength = 40,
                            maxLineGap = 7)

    averaged_lines = average_slope_intercept(frame, lines)
    line_image = display_lines(frame, averaged_lines)
    combo_image = cv2.addWeighted(frame, 0.8, line_image, 1, 2)
    cv2.imshow("results", combo_image)
    # binary_threshold(canny_image)
    # When the below two will be true and will press the 'q' on
    # our keyboard, we will break out from the loop

    # # wait 0 will wait for infinitely between each frames.
    # 1ms will wait for the specified time only between each frames
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# close the video file
cap.release()

# destroy all the windows that is currently on
cv2.destroyAllWindows()
