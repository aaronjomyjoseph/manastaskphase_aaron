import os
import re
import cv2
import numpy as np
import matplotlib.pyplot as plt

col_frames = os.listdir(r'C:/Users/Maximus/CSE/frames1/')
col_frames.sort(key=lambda f: int(re.sub('\D', '', f)))
col_images=[]
for i in col_frames:
    img = cv2.imread(r'C:/Users/Maximus/CSE/frames1/'+i)
    col_images.append(img)

print(len(col_images))
idx = 12
img=col_images[idx]

# plot frame
plt.figure(figsize=(10,10))
plt.imshow(col_images[idx])
plt.show()


# plot masked frame
plt.figure(figsize=(10,10))
plt.imshow(img, cmap="gray")
plt.show()
