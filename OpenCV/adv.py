"""
A more advanced detector than the previous one 
Steps:
    1. Undistort -> Raw images are normally distorted (appears differently than it actually is)
        https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html
    2. Warp -> Birds eye view
    3. Isolating Lane Lines (color filters + sobel)
    4. Find lane Lines
    5. Incorporate #4 and display final image
"""
import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import time

def distortion_correction():
    #termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TermCriteria_MAX_ITER, 30, 0.001)

    # object points - 3D coordinate matrix
    # 6 x 9 grid
    objp = np.zeros((6*9,3), np.float32)
    objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

    #storing object and img points from all images
    objpoints = [] #3d points in real world
    imgpoints = [] #2d point in img plane

    #calibration images
    FILE_PATH_IMGS = "OpenCV\calibration_imgs"
    images = os.listdir(FILE_PATH_IMGS)    

    for image in images:
        img = cv2.imread(f"{FILE_PATH_IMGS}/{image}")
        print(image)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        #chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (9,6), None)

        # if found refine and add object and image points
        if ret == True:
            objpoints.append(objp)
            # increasing accuracy of found corners
            corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    return (mtx, dist)

def warp(undist, mtx, dist):
    height, width = undist.shape[0], undist.shape[1]
    OFFSET = 100

    src = np.float32([
    (596, 447), # Top-left corner
    (190, 720), # Bottom-left corner
    (1125, 720), # Bottom-right corner
    (685, 447) # Top-right corner
    ])

    #what we want to warp - trapezoid shape - destination
    dst = np.float32([
        [OFFSET, 0],
        [OFFSET, height],
        [width - OFFSET, height],
        [width - OFFSET, 0]
    ])

    #transformation matrix -> normal to birds eye view
    M = cv2.getPerspectiveTransform(src, dst)
    #inverse transformation -> birds eye view to normal
    M_inv = cv2.getPerspectiveTransform(dst, src)
    #warped
    warped = cv2.warpPerspective(undist, M, (width, height), flags=cv2.INTER_LINEAR)

    return (warped, M_inv)

def binary_threshold(img):
    # #grayscale
    undist_test_img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #sobel x for lines that tend to be vertical - change in derivatives
    sobel_x = cv2.Sobel(undist_test_img_gray, cv2.CV_64F, 1, 0)
    #scale results b/w 0 and 255
    abs_sobel_x = np.absolute(sobel_x)
    scaled_sobel_x = np.uint8(255*abs_sobel_x / np.max(abs_sobel_x))
    #binary - only keep vals in margin of interest
    sobel_x_bin = np.zeros_like(scaled_sobel_x)
    sobel_x_bin[(scaled_sobel_x >= 20) & (scaled_sobel_x <= 120)]  = 1

    ### Yellow and white ###
    hls = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
    h, l, s = hls[:,:,0], hls[:,:,1], hls[:,:,2]
    #white
    white_bin = np.zeros_like(undist_test_img_gray)
    white_bin[(l >= 200) & (l <= 255)] = 1
    #yellow
    yellow_bin = np.zeros_like(s)
    yellow_bin[
        ((h >= 15) & (h <= 35)) &
        ((l >= 30) & (h <= 205)) &
        ((s >= 115) & (s <= 255))
    ] = 1

    bin1 = cv2.bitwise_or(white_bin, yellow_bin)
    bin_final = cv2.bitwise_or(sobel_x_bin, bin1)

    return bin_final

### LANE LINE DETECTION ###
def make_hist(warp_img):
    bottom_half = warp_img[warp_img.shape[0]//2:, :]
    return np.sum(bottom_half, axis=0)

def lane_pixels_histogram(binary_warped):
    histogram = make_hist(binary_warped)

    #left and right
    midpoint = np.int(histogram.shape[0]//2)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint

    #num of sliding windows
    nwindows = 9
    #width of windows
    margin = 100
    #threshold vak
    minpix = 50

    window_height = np.int(binary_warped.shape[0]//nwindows)
    #getting x and y positions of non zero pixels 
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])
    #keeping trach of curr positions
    leftx_current = leftx_base
    rightx_current = rightx_base

    #storing indices of left and right lane pixels
    left_lane_inds, right_lane_inds = [], []

    #sliding window
    for window in range(nwindows):
        # window boundaries
        win_y_low = binary_warped.shape[0] - (window + 1) * window_height
        win_y_high = binary_warped.shape[0] - window * window_height 
        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin
        win_xright_low = rightx_current - margin
        win_xright_high = rightx_current + margin

        #getting nonzero indixes w/in window boundaries
        good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xleft_low)
        & (nonzerox < win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xright_low)
        & (nonzerox < win_xright_high)).nonzero()[0]

        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)

        #checking is greater than threshold
        if len(good_left_inds) > minpix:
            leftx_current = np.int(np.mean(nonzerox[good_left_inds]))
        if len(good_right_inds) > minpix:        
            rightx_current = np.int(np.mean(nonzerox[good_right_inds]))
    
    #concatenating array of indices
    try:
        left_lane_inds = np.concatenate(left_lane_inds)
        right_lane_inds = np.concatenate(right_lane_inds)
    except ValueError:
        pass

    # left and right pixel positions
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds] 
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    return leftx, lefty, rightx, righty

def fit_poly_line(binary_warped, leftx, lefty, rightx, righty):
    # x and y vals for plotting
    ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0])
    #if left/right fit are None
    try:
        #fitting a curve second order polynomial
        left_fit = np.polyfit(lefty, leftx, 2)
        right_fit = np.polyfit(righty, rightx, 2)
        
        left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
        right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]
    except:
        print("NO LINE FITTED")
        left_fit, right_fit = None, None
        left_fitx = ploty**2 + ploty
        right_fitx = ploty**2 + ploty
    
    return left_fit, right_fit, left_fitx, right_fitx, ploty

def draw_poly_lines(binary_warped, left_fitx, right_fitx, ploty):
    #image to draw on
    out_img = np.dstack((binary_warped, binary_warped, binary_warped)) * 255
    window_img = np.zeros_like(out_img)

    margin = 100
    # Generate a polygon to illustrate the search window area
    # And recast the x and y points into usable format for cv2.fillPoly()
    left_line_window1 = np.array([np.transpose(np.vstack([left_fitx-margin, ploty]))])
    left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([left_fitx+margin, 
                              ploty])))])
    left_line_pts = np.hstack((left_line_window1, left_line_window2))
    right_line_window1 = np.array([np.transpose(np.vstack([right_fitx-margin, ploty]))])
    right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([right_fitx+margin, 
                              ploty])))])
    right_line_pts = np.hstack((right_line_window1, right_line_window2))

    # Draw the lane onto the warped blank image
    cv2.fillPoly(window_img, np.int_([left_line_pts]), (255, 0, 0))
    cv2.fillPoly(window_img, np.int_([right_line_pts]), (0, 255, 0))
    result = cv2.addWeighted(out_img, 1, window_img, 0.5, 0)
    
    return result

def project_lane_info(img, binary_warped, ploty, left_fitx, right_fitx, M_inv):
    # Create an image to draw the lines on
    warp_zero = np.zeros_like(binary_warped).astype(np.uint8)
    color_warp = np.dstack((warp_zero, warp_zero, warp_zero))
    
    # Recast the x and y points into usable format for cv2.fillPoly()
    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
    pts = np.hstack((pts_left, pts_right))
    
    # Draw the lane onto the warped blank image
    cv2.fillPoly(color_warp, np.int_([pts]), (0,255, 0))
    
    # Warp the blank back to original image space using inverse perspective matrix (Minv)
    newwarp = cv2.warpPerspective(color_warp, M_inv, (img.shape[1], img.shape[0]))
    
    # Combine the result with the original image
    out_img = cv2.addWeighted(img, 1, newwarp, 0.3, 0)
    
    return out_img

def main_pic():
    PATH = "straight_lines1.jpg"
    image = cv2.imread(PATH)
    image_cp = image.copy()
    cv2.imshow('i', image)
    cv2.waitKey(0)
    mtx, dist = distortion_correction()
    warped_img, M_inv = warp(image_cp, mtx, dist)
    cv2.imshow('warped', warped_img)
    cv2.waitKey(0)
    plt.imshow(warped_img)
    plt.show()
    bin_threshold_img = binary_threshold(image_cp)
    out1 = np.dstack((bin_threshold_img, bin_threshold_img, bin_threshold_img))*255
    plt.imshow(out1, cmap="gray")
    plt.show()
    binary_warped, M_inv = warp(bin_threshold_img, mtx, dist)

    leftx, lefty, rightx, righty = lane_pixels_histogram(binary_warped)
    left_fit, right_fit, left_fitx, right_fitx, ploty = fit_poly_line(binary_warped,leftx, lefty, rightx, righty)
    print(left_fit)
    out_img = draw_poly_lines(binary_warped, left_fitx, right_fitx, ploty)
    plt.imshow(out_img)
    plt.show()
    cv2.imshow("imgae", out_img)
    cv2.waitKey(0)

    prev_left_fit, prev_right_fit = left_fit, right_fit
    print('prev left fit: ', prev_left_fit)
    print('prev right fit: ', prev_right_fit)

    new_img = project_lane_info(cv2.cvtColor(image_cp, cv2.COLOR_BGR2RGB), binary_warped, ploty, left_fitx, right_fitx, M_inv)

    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(24, 9))
    f.tight_layout()
    ax1.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    ax1.set_title('Original Image', fontsize=20)
    ax2.imshow(new_img, cmap='gray')
    ax2.set_title('Image With Lane Marked', fontsize=20)
    plt.show()

def main_video():
    VID_PATH = "vid2.mp4"
    cap = cv2.VideoCapture(r'C:\Users\Maximus\manastaskphase_aaron\data\harder_challenge_video.mp4')
    mtx, dist = distortion_correction()
    while (cap.isOpened()):
        _, frame = cap.read()
        frame = cv2.undistort(frame, mtx, dist, None, mtx)
        bin_threshold_img = binary_threshold(frame)
        binary_warped, M_inv = warp(bin_threshold_img, mtx, dist)

        leftx, lefty, rightx, righty = lane_pixels_histogram(binary_warped)
        left_fit, right_fit, left_fitx, right_fitx, ploty = fit_poly_line(binary_warped,leftx, lefty, rightx, righty)

        new_img = project_lane_info(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB), binary_warped, ploty, left_fitx, right_fitx, M_inv)

        cv2.imshow("vid", new_img)
        if cv2.waitKey(1) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main_video()
    # main_pic()
